//아코디언
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

//메인 swiper
$(document).ready(function(){
  const mainSwiper = new Swiper('.main-swiper', {
    loop: true,
    autoplay: {
      delay: 3000,
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },  
  });  
});

//견적 swiper
$(document).ready(function(){
  if (window.outerWidth > 768) {    
    const rankSwiper = new Swiper('.rank-swiper', {
      slidesPerView: 3,
      spaceBetween: 72,
    });
  } else{
    const rankSwiper = new Swiper('.rank-swiper', {
      slidesPerView: "auto",
      spaceBetween: 20,
    });
  } 
  $(window).resize(function (){
      if (window.outerWidth > 768) {    
        const rankSwiper = new Swiper('.rank-swiper', {
          slidesPerView: 3,
          spaceBetween: 72,
        });
      } else{
        const rankSwiper = new Swiper('.rank-swiper', {
          slidesPerView: "auto",
          spaceBetween: 20,
        });
      } 
  });
});




//견적 swiper 순위 외 선택시
$(document).ready(function(){
  
    const rankSwiper = new Swiper('.listOne', {
      slidesPerView: 1,
    });
    
});


$(document).ready(function(){   

  //예상견적 링크 active     
  $(".recipt").click(function () {
    $(".recipt").removeClass("active");     
    $(this).toggleClass("active");     
    $(".price-info").addClass("active");     
  });

  //tab(예상견적조회 리스트pop)
  var $tabs = $('.tab-btn a'); 
  $tabs.on('click', function(e) {
      e.preventDefault();
      $tabs.removeClass('active');
      $(this).addClass('active');
      $('.tab-cont-box').hide();
      $(this.hash).show();
  });
});
