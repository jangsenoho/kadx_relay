$(document).ready(function(){        
    
    // 비밀번호 확인    
    $('.pw1,.pw2').keyup(function () {
        var pwd1 = $("#password_1").val();
        var pwd2 = $("#password_2").val();
  
        if ( pwd1 != '' && pwd2 == '' ) {
            null;
        } else if (pwd1 != "" || pwd2 != "") {
            if (pwd1 == pwd2) {
                $('.pw2').parents(".infoLine").next('.passNot').css('display','none');
                $('.pw2').parents(".infoLine").removeClass('danger');
                $('.pw2').parents(".infoLine").removeClass('m-b-10'); 
                $('.pw2').parents(".infoLine").addClass('m-b-30');                
            } else {
                $('.pw2').parents(".infoLine").next('.passNot').css('display','block');
                $('.pw2').parents(".infoLine").addClass('danger');
                $('.pw2').parents(".infoLine").addClass('m-b-10'); 
                $('.pw2').parents(".infoLine").removeClass('m-b-30');         
            }
        }
    });

    //마이페이지 개인정보관리 탭
    $(".tab_content").hide();
    $(".tab_content:first").show();

    $("ul.tabs li").click(function () {
        $("ul.tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab_content").hide()
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).show()       
    });



    //품목모달 탭
    $(".mtab_content").hide();
    $(".mtab_content:first").show();

    $("ul.mtabs li").click(function () {
        $("ul.mtabs li").removeClass("active");
        $(this).addClass("active");
        $(".mtab_content").hide()
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).show()       
    });


    //검색품목 선택 토글
    $('.resultItem').click(function(){
  
        $(this).toggleClass('active');
        $('.resultItem').not(this).removeClass('active');

    });

    


    //모바일메뉴 토글
    $('.menuBtn').on('click',function(){
        $(this).toggleClass('enable');
        $(this).parent().siblings('.mobile-menu').toggleClass('on');
    });

    

    //인증번호 활성화 스크립트
    $(".sendCerty").bind("click", function(){
        $("#certyInputy").attr("disabled", false);
    });



    //파렛트 스위치 토글
    $('.switch').click(function() {
        if ($(this).hasClass('off')){
            $(this).parent().find('input:checkbox').attr('checked', false);	
            $(this).removeClass('off').addClass('on');	
            $(this).parents('.infoLine').siblings('.carInfo').addClass('visible');
            $(this).parents('.infoLine').siblings('.selPallete').removeClass('disabled');
        } else {
            $(this).parent().find('input:checkbox').attr('checked', true);
            $(this).removeClass('on').addClass('off');
            $(this).parents('.infoLine').siblings('.carInfo').removeClass('visible');
            $(this).parents('.infoLine').siblings('.selPallete').addClass('disabled');
        }	
    });
});


    // 입력한 검색어 삭제
    var clearInput = function(obj) {
        obj.parentNode.querySelector('input').value = ""
    }


    //아이디 중복확인 모달
    $(document).on('click','#idCheck', function() {
        $('.modal').addClass('visible');
	});


    //은행선택 모달
    $(document).on('click','#searBank', function() {
        $('.bankPop').addClass('visible');
	});


    //이용약관 모달
    $(document).on('click','#personBtn', function() {
        $('.personPop').addClass('visible');
	});

    
    //품목·품종 검색 모달
	$(document).on('click','.searPro', function() {
        $('.proPop').addClass('visible');
	});

    
    //등급 모달
	$(document).on('click','.selGrade', function() {
        $('.gradePop').addClass('visible');
	});

    //물량 모달
	$(document).on('click','.selQuantity', function() {
        $('.quantityPop').addClass('visible');
	});


    //파렛트 모달
	$(document).on('click','.selPallete', function() {
        $('.palettePop').addClass('visible');
	});

    //차량선택 모달
	$(document).on('click','.carSel', function() {
        $('.selCar').addClass('visible');
	});



    //예악취소 모달
	$(document).on('click','.mpBtn', function() {
        $('.myPop1').addClass('visible');
	});

    //실 수취가격 모달
	$(document).on('click','.mpBtn2', function() {
        $('.myPop2').addClass('visible');
	});

    


    //이용약관내 리스트 보이기-ys
    $(document).on('click','.listShow-btn', function() {
        $(".listShow").toggleClass("active");
	});

    //예상견적 조회 리스트 더보기 팝업-ys
	$(document).on('click','.more', function() {
        $('.rankPop').addClass('visible');
	});

    //예상 가격 상세 리스트 팝업-ys
    $(document).on('click','.detailInfo', function() {
        $('.detailPricePop').addClass('visible');
	});



    //예상 가격 상세 리스트 팝업만 끄기-sh 
    $(document).on('click','.priceClose', function() {
        $('.priceChkPop').removeClass('visible');
	});
    

    //예상 가격 상세 리스트 팝업만 끄기-sh 
    $(document).on('click','.detailPclose', function() {
        $('.detailPricePop').removeClass('visible');
	});
    
    //예상 가격 상세 리스트 팝업만 끄기-sh 
    $(document).on('click','.inquiryClose', function() {
        $('.inquiryPop').removeClass('visible');
	});
    

    $(document).on('click','.login-open-pop', function() {
        $('.modal.login-pop').addClass('visible');
	});

    //생산자 수취 예상가격 팝업 -ys
    $(document).on('click','.showTotal', function() {
        $('.priceChkPop').addClass('visible');
	});


    
    // 공통 모달 닫기
	$(document).on('click','.closeModal, #closeModal, .applyBtn', function() {
        $('.modal').removeClass('visible');
	});



    //pc버전 드랍다운 메뉴		
	$(document).on('click','.dropdown', function() {
		$(this).parents().siblings().find('.dropdown').removeClass('open');
	});

	$(document).on('click','.dropdown > .caption', function() {
		$(this).parent().toggleClass('open');
	});

	$(document).on('click','.dropdown > .list > .item', function() {
		$('.dropdown > .list > .item').removeClass('selected');
		$(this).addClass('selected').parent().parent().removeClass('open').children('.caption').text( $(this).text() );
	});


	$(document).on('click', function(evt) {
		if ( $(evt.target).closest(".dropdown > .caption").length === 0 ) {
			$('.dropdown').removeClass('open');
		}
	});


    $(function() {
    
            var wi = $(window).width();
            if (wi >= 1200) {
                $('#idCheck').on('click',function(){
                    $(this).text('중복확인 완료');
                });
            }
            else {
                $('#idCheck').on('click',function(){
                    $(this).css('color','#0682e2');
                });
            }
    });
