
 
    $(function() {
        var today = new Date(),
          endGo = new Date(),
          $from = $("#StartDate"),
          $to = $("#EndDate");
      
          endGo.setDate(today.getDate() + 30);
      
        $from.datepicker({   
          dateFormat: 'yy-mm-dd',
          showOn: "both",
          buttonImage: "../images/frn/calendar.svg",
          onSelect: function(dateText) {
            $to.datepicker("option", "minDate", dateText);
          }
        }).datepicker('setDate', today);
      
        $to.datepicker({
            dateFormat: 'yy-mm-dd',
            showOn: "both",
            buttonImage: "../images/frn/calendar.svg",
          onSelect: function(dateText) {
            $from.datepicker("option", "maxDate", dateText);
          }
        }).datepicker('setDate', endGo);
      
      });


  