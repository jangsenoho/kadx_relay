

// 로그인페이지 비밀번호 show/hide toggle

function myFunction() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
}



// 체크박스 셀렉트

jQuery(function() {
	jQuery('[data-check-pattern]').checkAll();
});

(function($) {
	'use strict';
	
	$.fn.checkAll = function(options) {
		return this.each(function() {
			var mainCheckbox = $(this);
			var selector = mainCheckbox.attr('data-check-pattern');
			var onChangeHandler = function(e) {
				var $currentCheckbox = $(e.currentTarget);
				var $subCheckboxes;
				
				if ($currentCheckbox.is(mainCheckbox)) {
					$subCheckboxes = $(selector);
					$subCheckboxes.prop('checked', mainCheckbox.prop('checked'));
				} else if ($currentCheckbox.is(selector)) {
					$subCheckboxes = $(selector);
					mainCheckbox.prop('checked', $subCheckboxes.filter(':checked').length === $subCheckboxes.length);
				}
			};
			
			$(document).on('change', 'input[type="checkbox"]', onChangeHandler);
		});
	};
}(jQuery));




// 버튼 클릭시 뒤로 가기

function goBack(){
    window.history.back();
}





$(document).ready(function(){
    
    
    // 인풋 활성/비활성화 토글
    
    $("#switch").on("click", function(){
        var toggle = $("#changeInput").prop("disabled");

        $("#changeInput").prop("disabled", !toggle );
        $(this).siblings('.tableButton').addClass('on');
        $(this).remove();
    });

    
    
    
    // 모달 내의 탭
    
    var tabBtn = $("#tab-btn > ul > li");
    var tabCont = $("#tab-cont > div");

    tabCont.hide().eq(0).show();

    tabBtn.click(function(){
        var target = $(this);
        var index = target.index();
        //alert(index);
        tabBtn.removeClass("active");
        target.addClass("active");
        tabCont.css("display","none");
        tabCont.eq(index).css("display", "block");
    });

    
    // 모달 오픈
    $('.modalOpen').on('click',function(){
        $('.modalWrap').addClass('open');
        $('body').addClass('modalOpen')
    });
    
    
    // 모달 닫기
    $('.closeModal').on('click',function(){
        $('.modalWrap').removeClass('open');
        $('body').removeClass('modalOpen')
    });
    
    
    
    //파일 업로드
    
    var inputs = document.querySelectorAll('.file-input')

    for (var i = 0, len = inputs.length; i < len; i++) {
    customInput(inputs[i])
    }

    function customInput (el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
        fileInput.onmouseout = function () {
            if (!fileInput.value) return

            var value = fileInput.value.replace(/^.*[\\\/]/, '')
            el.className += ' -chosen'
            label.innerText = value
        }
    }
    
    
    
    // 비밀번호 확인
    
    $('.pw').keyup(function () {
        var pwd1 = $("#password_1").val();
        var pwd2 = $("#password_2").val();
  
        if ( pwd1 != '' && pwd2 == '' ) {
            null;
        } else if (pwd1 != "" || pwd2 != "") {
            if (pwd1 == pwd2) {
                $("#alert-success").css('display', 'block');
                $("#alert-danger").css('display', 'none');
            } else {
                $("#alert-success").css('display', 'none');
                $("#alert-danger").css('display', 'block');
            }
        }
    });
});




                        
